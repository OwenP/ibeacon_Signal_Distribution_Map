﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    var beacon = {};

    // History of enter/exit events.
    var mRegionEvents = [];

    // Nearest ranged beacon.
    var mNearestBeacon = null;

    // Timer that displays nearby beacons.
    var mNearestBeaconDisplayTimer = null;

    // Background flag.
    var mAppInBackground = false;

    // Background notification id counter.
    var mNotificationId = 0;
    var d1 = 0;
    var d2 = 0;
    var d3 = 0;
    var d4 = 0;
    var d1_old = 0;
    var d2_old = 0;
    var d3_old = 0;
    var d4_old = 0;
    var y1 = 0;
    var y2 = 0;
    var y3 = 0;
    var y4 = 0;


    function csv() {
        return Array.prototype.slice.call(arguments).join(",");
    }
    function timeStr(timestamp) {
        return moment.unix(timestamp / 1000).format("YYYY-MM-DD HH:mm:ss.SSS");
    }
    function getFile(path, options, onSuccess, onError) {
        var elements = path.split("/");
        function next(i) {
            return function (file) {
                if (elements.length == i) return onSuccess(file);
                file[(i == (elements.length - 1)) ? "getFile" : "getDirectory"](elements[i], options, next(i + 1), onError);
                return false;
            }
        }
        requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            next(0)(fileSystem.root);
        }, onError);
    }
    function logger(file) {
        var writing = false;
        var buffer = [];
        function onError(e) {
            console.log(e);
        }
        function write() {
            if (writing) return false;
            getFile(file, { create: true }, function (fileEntry) {
                fileEntry.createWriter(function (writer) {
                    writing = true;
                    try {
                        writer.seek(writer.length);
                        while (buffer.length > 0) {
                            writer.write(buffer.shift() + "\n");
                        }
                    } finally {
                        writing = false;
                    }
                });
            }, onError);
            return true;
        }
        function log(line) {
            buffer.push(line);
            write();
        }
        return {
            log: log
        };
    }



    // Mapping of region event state names.
    // These are used in the event display string.
    var mRegionStateNames =
	{
	    'CLRegionStateInside': 'Enter',
	    'CLRegionStateOutside': 'Exit'
	};

    // Here monitored regions are defined.
    // TODO: Update with uuid/major/minor for your beacons.
    // You can add as many beacons as you want to use.
    var mRegions =
	[
        // kontackt ibeacons
        {
            id: 'CIsx',
            uuid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
            major: 21623,
            minor: 1
        },
        {
            id: 'qsuD',
            uuid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
            major: 1149,
            minor: 2
        },
        {
            id: 'jfyQ',
            uuid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
            major: 266,
            minor: 3
        },
        {
            id: 'm9Xa',
            uuid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
            major: 299,
            minor: 4
        }
	];

    // Region data is defined here. Mapping used is from
    // region id to a string. You can adapt this to your
    // own needs, and add other data to be displayed.
    // TODO: Update with major/minor for your own beacons.
    var mRegionData =
	{
	    //		'region1': 'Region One',
	    //		'region2': 'Region Two'
	    'CIsx': 'CIsx',
	    'qsuD': 'qsuD',
	    'jfyQ': 'jfyQ',
	    'm9Xa': 'm9Xa'
	};


    document.removeEventListener('deviceready', onDeviceReady, false);
    document.addEventListener("deviceready", onDeviceReady, false);
    //document.addEventListener('pause', onAppToBackground, false);
    //document.addEventListener('resume', onAppToForeground, false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        //document.addEventListener( 'pause', onPause.bind( this ), false );
        //document.addEventListener( 'resume', onResume.bind( this ), false );
        // Logging data to a text file
        //requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess, onError);
        // GPS location 
        //navigator.geolocation.getCurrentPosition(onGeolocationSuccess, onGeolocationError);
        setUpFolders();
        startMonitoringAndRanging();
        drawFloorPlan();
        startNearestBeaconDisplayTimer();
        displayRegionEvents();
    };

    function setUpFolders() {


        requestFileSystem(LocalFileSystem.PERSISTENT, 0, FirstonFileSystemSuccess, FirstonFileSystemFail);

        function FirstonFileSystemSuccess(fileSystem) {
            var rootDirectoryEntry = fileSystem.root;
            rootDirectoryEntry.getDirectory(appFolderName, { create: true }, FirstonGetAppFolderSuccess, FirstonGetAppFolderFail);
        }

        function FirstonGetAppFolderSuccess(ibeaconLogDirectoryEntry) {
            ibeaconLogDirectoryEntry.getDirectory(logFolderNAme, { create: true }, FirstonGetLogFolderSuccess, FirstonGetLogFolderFail);
            //ibeaconLogDirectoryEntry.removeRecursively(
            //        function removesucess() {
            //            console.log("i'm in removesucess");
            //        },
            //        function removefail(error) {
            //            console.log("i'm in removefail");
            //        })         
        }

        function FirstonGetLogFolderSuccess(logfile) {
            console.log('Log File Exists');
        }


        function FirstonGetLogFolderFail(error) {
            console.log("Error occurred while getting a pointer to file. Error code is: " + error.code);
        }

        function FirstonGetAppFolderFail(error) {
            if (error.code !== FileError.PATH_EXISTS_ERR) {
                displayAlertMessage("Error occurred while creating folder/getting pointer to folder ('" + appFolderName + "') in the application storage directory. Error code is: " + error.code);
            } else {
                console.log(error);
            }
        }

        function FirstonFileSystemFail(evt) {
            displayAlertMessage("File System Failed: " + evt.target.error.code);
        }

    }

    var appFolderName = "ibeaconlog";
    var logFolderNAme = "log";
    var logFileName = "log";


    var accuracy = null;
    var altitudeAccuracy = null;
    var altitude = null;
    var heading = null;
    var speed = null;
    var lattitude = null;
    var longitude = null;
    var timestamp = null;
    var starttimestamp = null;
    var mainLogger = null;
    var sync = "false";


    //function createFolders(rootDirectoryEntry) {
    //    //console.log("setting up folders");
    //    //get app folder '" + rootDirectoryEntry.fullPath + appFolderName + "' - create it if it does not exist...
    //    rootDirectoryEntry.getDirectory(
    //        appFolderName,
    //        { create: true, exclusive: false },
    //        function onGetAppFolderSuccess(ibeaconLogDirectoryEntry) {
    //            console.log("Im in onGetAppFolderSuccess");
    //            //get subfolder '"+ logFolderNAme+"' in app folder - create it if it does not exist...
    //            ibeaconLogDirectoryEntry.getDirectory(
    //                logFolderNAme,
    //                { create: true, exclusive: false },
    //                function onGetLogFolderSuccess(logDirectoryEntry) {
    //                    //do nothing more
    //                    console.log("I'm in onGetPDFFolderSuccess");
    //                    logDirectoryEntry.getFile(logFileName, { create: true, exclusive: false }, function (fileEntry) {
    //                        //lets write something into the file
    //                        fileEntry.createWriter(function (writer) {
    //                            writer.write("This is the text inside readme file");
    //                        }, function (error) {
    //                            console.log("Error occurred while writing to file. Error code is: " + error.code);
    //                        });
    //                    }, function (error) {
    //                        console.log("Error occurred while getting a pointer to file. Error code is: " + error.code);
    //                    });
    //                },
    //                function onGetLogFolderFail(error) {
    //                    console.log("I'm in onGetPDFFolderFail");
    //                    if (error.code !== FileError.PATH_EXISTS_ERR) {
    //                        console.log("Error occurred while creating folder/getting pointer to folder ('" + appFolderName + "') in the application storage directory. Error code is: " + error.code);
    //                    } else {
    //                        //printFileError(error);
    //                    }
    //                }
    //            );
    //        },
    //        function onGetAppFolderFail(error) {
    //            console.log("Im in onGetAppFolderFail");
    //            if (error.code !== FileError.PATH_EXISTS_ERR) {
    //                console.log("Error occurred while creating folder/getting pointer to folder ('" + appFolderName + "') in the application storage directory. Error code is: " + error.code);
    //            } else {
    //                //printFileError(error);
    //            }
    //        }
    //    );
    //}

    //function onSuccess(fileSystem) {
    //    //fileSystem.root points to the application storage directory
    //    var rootDirectoryEntry = fileSystem.root;
    //    console.log(rootDirectoryEntry);
    //    //lets create a file namesd readme.txt. getFile method actually creates a file and returns a pointer(FileEntry) if it doesn't exist otherwise just returns a pointer to it. It returns the file pointer as callback parameter.
    //    rootDirectoryEntry.getDirectory(
    //        appFolderName,
    //        { create: false, exclusive: false },
    //        function onGetAppFolderSuccess(ibeaconLogDirectoryEntry) {
    //            console.log("I'm in onGetAppFolderSuccess");
    //            ibeaconLogDirectoryEntry.removeRecursively(
    //                function removeSucess() {
    //                    createFolders(rootDirectoryEntry);
    //                    console.log("I'm in removeSucess");
    //                },
    //                function removeFail(error) {
    //                    console.log("I'm in removeFail");
    //                    createfolders(rootdirectoryentry);
    //                }
    //           );
    //        },
    //        function onGetAppFolderFail(error) {
    //            console.log("I'm in onGetAppFolderFail");
    //            //this is triggered if the du700mobile folder doesn't exist
    //            console.log("onGetAppFolderfail(): ");
    //            console.log(error);
    //            //printFileError(error);
    //            createFolders(rootDirectoryEntry);
    //        });
    //}

    //function onError(fileSystem) {
    //    //displayAlertMessage("File System Failed: " + evt.target.error.code);
    //}

    function onAppToBackground() {
        //////console.log("I'm in onAppToBackground");
        mAppInBackground = true;
        stopNearestBeaconDisplayTimer();
    }

    function onAppToForeground() {
        ////console.log("I'm in onAppToForeground");
        mAppInBackground = false;
        startNearestBeaconDisplayTimer();
        displayRegionEvents();
    }

    function startNearestBeaconDisplayTimer() {
        ////console.log("I'm in startNearestBeaconDisplayTimer");
        mNearestBeaconDisplayTimer = setInterval(displayNearestBeacon, 1000);
    }

    function stopNearestBeaconDisplayTimer() {
        ////console.log("I'm in stopNearestBeaconDisplayTimer");
        clearInterval(mNearestBeaconDisplayTimer);
        mNearestBeaconDisplayTimer = null;
    }

    function startMonitoringAndRanging() {

        ////console.log("I'm in startMonitoringAndRanging");

        function onDidDetermineStateForRegion(result) {
            //            //console.log("Im in onDidDetermineStateForRegion");
            //console.log("Result" + JSON.stringify(result));
            saveRegionEvent(result.state, result.region.identifier);
            displayRecentRegionEvent();
        }

        function onDidRangeBeaconsInRegion(result) {
            ////console.log("Im in onDidRangeBeaconsInRegion");
            updateNearestBeacon(result.beacons);
            //            draw(result.beacons);
            //            drawOffice(result.beacons);
            RorysWeightedSignalAverageMethod(result.beacons);
            displayDistancestoiBeacons(result.beacons);
            if (isRecording == true) {
                logData(result.beacons);
            }
        }

        function onError(errorMessage) {
            ////console.log('Monitoring beacons did fail: ' + errorMessage);
        }

        // Request permission from user to access location info.
        cordova.plugins.locationManager.requestAlwaysAuthorization();

        // Create delegate object that holds beacon callback functions.
        var delegate = new cordova.plugins.locationManager.Delegate();



        // Start monitoring and ranging beacons.
        startMonitoringAndRangingRegions(mRegions, onError);

        // Set delegate functions.
        delegate.didDetermineStateForRegion = onDidDetermineStateForRegion;
        delegate.didRangeBeaconsInRegion = onDidRangeBeaconsInRegion;

        cordova.plugins.locationManager.setDelegate(delegate);

    }

    function startMonitoringAndRangingRegions(regions, errorCallback) {
        ////console.log("I'm in startMonitoringAndRangingRegions");
        // Start monitoring and ranging regions.
        for (var i in regions) {
            startMonitoringAndRangingRegion(regions[i], errorCallback);
        }
    }

    function startMonitoringAndRangingRegion(region, errorCallback) {
        ////console.log("I'm in startMonitoringAndRangingRegion");
        ////console.log("Region" + JSON.stringify(region));
        ////console.log("Region" + JSON.stringify(errorCallback));

        var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(
            region.id,
            region.uuid,
            region.major,
            region.minor);
        //        
        //     var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(
        //			region.id,
        //			region.uuid);
        //        
        //console.log("Region" + JSON.stringify(beaconRegion));

        ////console.log("Im here before startRangingBeaconsInRegion");
        // Start ranging.
        cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion)
            .fail(errorCallback)
            .done();

        ////console.log("Im here before startMonitoringForRegion");
        // Start monitoring.
        cordova.plugins.locationManager.startMonitoringForRegion(beaconRegion)
            .fail(errorCallback)
            .done();
    }

    function saveRegionEvent(eventType, regionId) {
        // Save event.
        ////console.log("Im in saveRegionEvent");
        mRegionEvents.push(
        {
            type: eventType,
            time: getTimeNow(),
            regionId: regionId
        });

        // Truncate if more than ten entries.
        if (mRegionEvents.length > 10) {
            mRegionEvents.shift();
        }
    }

    function getBeaconId(beacon) {
        ////console.log("I'm in isSameBeacon");
        return beacon.uuid + ':' + beacon.major + ':' + beacon.minor;
    }

    function isSameBeacon(beacon1, beacon2) {
        ////console.log("I'm in isSameBeacon");
        return getBeaconId(beacon1) == getBeaconId(beacon2);
    }

    function isNearerThan(beacon1, beacon2) {
        ////console.log("I'm in isNearerThan");
        return beacon1.accuracy > 0
            && beacon2.accuracy > 0
            && beacon1.accuracy < beacon2.accuracy;
    }

    function updateNearestBeacon(beacons) {
        ////console.log("I'm in updateNearestBeacon")
        for (var i = 0; i < beacons.length; ++i) {
            var beacon = beacons[i];
            if (!mNearestBeacon) {
                mNearestBeacon = beacon;
            }
            else {
                if (isSameBeacon(beacon, mNearestBeacon) ||
                    isNearerThan(beacon, mNearestBeacon)) {
                    mNearestBeacon = beacon;
                }
            }
        }
    }

    function displayNearestBeacon() {
        ////console.log("I'm in displayNearestBeacon");
        if (!mNearestBeacon) { return; }

        // Clear element.
        $('#beacon').empty();

        // Update element.
        var element = $(
            '<li>'
            + '<strong>Nearest Beacon</strong><br />'
            + 'UUID: ' + mNearestBeacon.uuid + '<br />'
            + 'Major: ' + mNearestBeacon.major + '<br />'
            + 'Minor: ' + mNearestBeacon.minor + '<br />'
            + 'Proximity: ' + mNearestBeacon.proximity + '<br />'
            + 'Distance: ' + mNearestBeacon.accuracy + '<br />'
            + 'RSSI: ' + mNearestBeacon.rssi + '<br />'
            + '</li>'
            );
        $('#beacon').append(element);
    }

    function displayRecentRegionEvent() {
        ////console.log("I'm in displayRecentRegionEvent");
        if (mAppInBackground) {
            // Set notification title.
            var event = mRegionEvents[mRegionEvents.length - 1];
            if (!event) { return; }
            var title = getEventDisplayString(event);

            // Create notification.
            cordova.plugins.notification.local.schedule({
                id: ++mNotificationId,
                title: title
            });
        }
        else {
            displayRegionEvents();
        }
    }

    function displayRegionEvents() {
        ////console.log("I'm in displayRegionEvents");
        // Clear list.
        $('#events').empty();

        // Update list.
        for (var i = mRegionEvents.length - 1; i >= 0; --i) {
            var event = mRegionEvents[i];
            var title = getEventDisplayString(event);
            var element = $(
                '<li>'
                + '<strong>' + title + '</strong>'
                + '</li>'
                );
            $('#events').append(element);
        }

        // If the list is empty display a help text.
        if (mRegionEvents.length <= 0) {
            var element = $(
                '<li>'
                + '<strong>'
                + 'Waiting for region events, please move into or out of a beacon region.'
                + '</strong>'
                + '</li>'
                );
            $('#events').append(element);
        }
    }

    function getEventDisplayString(event) {
        ////console.log("I'm in getEventDisplayString");
        return event.time + ': '
            + mRegionStateNames[event.type] + ' '
            + mRegionData[event.regionId];
    }

    function getTimeNow() {
        ////console.log("I'm in getTimeNow");
        function pad(n) {
            return (n < 10) ? '0' + n : n;
        }

        function format(h, m, s) {
            return pad(h) + ':' + pad(m) + ':' + pad(s);
        }

        var d = new Date();
        return format(d.getHours(), d.getMinutes(), d.getSeconds());
    }

    function draw(beacons) {
        //context.clearRect(0,0, 400,200);
        //////console.log('im in drawB1');

        //    var beacon = beacons[i];


        ////console.log(JSON.stringify(beacons));

        //    if (beacons.length !=0){
        //        ////console.log('uuid: \t'+ beacons[0].uuid);
        //    }
        //    





        //    function toDegrees (angle) {
        //        return angle * (180 / Math.PI);
        //    }
        //    
        ////console.log("beacons.length" + beacons.length);
        if (beacons.length != 0) {

            for (var i = 0; i < beacons.length; ++i) {
                ////console.log('i - \t' + i);
                var beacon = beacons[i];
                ////console.log('beacon' +JSON.stringify( beacon));
                var beacon_uuid = beacon.uuid;
                switch (beacon_uuid) {
                    case 'b9407f30-f5f8-466e-aff9-25556b57fe6d':
                        d1 = beacon.accuracy;
                        y1 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    case '8492e75f-4fd6-469d-b132-043fe94921d8':
                        d2 = beacon.accuracy;
                        y2 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    default:
                        d1 = 0;
                        d2 = 0;
                }
            }
            d1_old = d1;
            d2_old = d2;
        }
        else if (beacons.length == 0) {
            d1 = d1_old;
            d2 = d2_old
        }

        // d2 is the virtual estimote
        // d1 is the other beacon   

        ////console.log("y1 : \t" + y1);
        ////console.log("y2 : \t" + d2);

        var canvas = document.getElementById('myCanvas');
        var width = canvas.width;
        var height = canvas.height;
        var context = myCanvas.getContext('2d');




        // calculate the distance between the beacons based on signal strenght
        var x = width / 2.0; // specifically for two beacons
        var y = 0;
        if ((y1 != 0) || (y2 != 0)) {
            y = Math.min(y1, y2) * 46.0;
            ////console.log("y : \t" + y);
            ////console.log("height/2: \t" + height/2.0);
        };




        // position  of receiver 
        context.beginPath();
        context.arc(x, y, 5, 0, 2 * Math.PI, true);
        context.strokeStyle = "black";
        context.lineWidth = 1;
        context.stroke();
        context.closePath();
        context.fill();

        // beacon 1 (not estimote virtual beacon) arc
        context.clearRect(0, 0, width, height);

        context.beginPath();
        context.arc(width / 2.0, 0, d1 * 46.0, 0, 2 * Math.PI, true);
        context.strokeStyle = "orange";
        context.lineWidth = 5;
        context.stroke();
        context.closePath();

        // beacon 2 arc
        context.beginPath();
        context.arc(width / 2.0, height, d2 * 46, 0, Math.PI, true);
        //    context.arc(width/2.0,height,80,0,Math.PI,true);
        context.strokeStyle = "red";
        context.lineWidth = 5;
        context.stroke();
        context.closePath();


        // draw a ibeacon
        context.beginPath();
        context.fillStyle = "#0000ff";
        // Draws a circle of radius 20 at the coordinates 100,100 on the canvas
        context.arc(width / 2.0, 0, 5, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
        // draw a ibeacon
        context.beginPath();
        context.fillStyle = "#0000ff";
        // Draws a circle of radius 20 at the coordinates 100,100 on the canvas
        context.arc(width / 2.0, height, 5, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();


        // Boundary Logic
        //if( x<0 || x>400) dx=-dx; 
        //if( y<0 || y>200) dy=-dy; 
        //x+=dx; 
        //y+=dy;
    }

    function drawFloorPlan() {
        var canvas = document.getElementById('myCanvas');
        var context = canvas.getContext('2d');
        context.fillStyle = "grey";
        context.fillRect(0, 0, canvas.width, canvas.height);
        // drawing the beacons
        context.beginPath();
        context.arc(0, 0, 10, 0, 2 * Math.PI, true);
        context.strokeStyle = "green";
        context.fillStyle = "green";
        context.lineWidth = 1;
        context.stroke();
        context.fill();
        context.closePath();

        context.beginPath();
        context.arc(0, canvas.height, 10, 0, 2 * Math.PI, true);
        context.strokeStyle = "green";
        context.fillStyle = "green";
        context.lineWidth = 1;
        context.stroke();
        context.fill();
        context.closePath();

        context.beginPath();
        context.arc(canvas.width, canvas.height, 10, 0, 2 * Math.PI, true);
        context.strokeStyle = "green";
        context.fillStyle = "green";
        context.lineWidth = 1;
        context.stroke();
        context.fill();
        context.closePath();

        context.beginPath();
        context.arc(canvas.width, 0, 10, 0, 2 * Math.PI, true);
        context.strokeStyle = "green";
        context.fillStyle = "green";
        context.lineWidth = 1;
        context.stroke();
        context.fill();
        context.closePath();

        //var imageObj = new Image();

        //imageObj.onload = function () {
        //    context.drawImage(imageObj, 0, 0, canvas.width, canvas.clientHeight);
        //};

        //imageObj.src = 'office beacons setup.png';
    }

    function drawOffice(beacons) // main one that calls trilateration
    {
        var canvas = document.getElementById('myCanvas');


        if (beacons.length != 0) {

            for (var i = 0; i < beacons.length; ++i) {
                ////console.log('i - \t' + i);
                var beacon = beacons[i];
                ////console.log('beacon' +JSON.stringify( beacon));
                switch (beacons[i].minor) {
                    case '1': // CLsX
                        d1 = beacon.accuracy;
                        y1 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    case '2':
                        d2 = beacon.accuracy;
                        y2 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    case '3':
                        d3 = beacon.accuracy;
                        y3 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    default:
                        d1 = 0;
                        d2 = 0;
                        d3 = 0;
                }
            }
            //            d1_old = d1;
            //            d2_old = d2;
            //            d3_old = d3;
        }
        //    else if (beacons.length == 0){
        //            d1 = d1_old; 
        //            d2 = d2_old;
        //            d3 = d3_old;
        //    }
        //        

        //     var i = 0;
        //     var j = canvas.width;    
        //     var x = (Math.pow((canvas.height/2.4) * d1,2) - Math.pow((canvas.height/2.4)*d2,2) + Math.pow(canvas.height,2)) / 2*canvas.height;
        //        
        //     var y = ( Math.pow((canvas.width/7.8) * d1,2) - Math.pow((canvas.width/7.8) * d3,2) -  Math.pow(i,2) +  Math.pow(j,2) / 2*j ) - (i/j)*x
        //     
        //     //console.log("canvas.height: \t" + canvas.height + "\t canvas.width: \t" + canvas.width);
        //     //console.log("x: \t" + x + "\t y: \t" + y);
        //              
        if (beacons.length != 0) {
            var xa = 0;
            var ya = 0;
            var xb = canvas.height * (2.4 / 150);
            var yb = 0;
            var xc = 0;
            var yc = canvas.width * (3.0 / 300);
            var ra = d1
            var rb = d2
            var rc = d3
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //       
            var p1 = { x: xa, y: ya, z: 0, r: ra };
            var p2 = { x: xb, y: yb, z: 0, r: rb };
            var p3 = { x: xc, y: yc, z: 0, r: rc };

            //       var p1 = {x:69, y:0,  r:69, z:0}
            //var p2 = {x:0,  y:50, r:50, z:0};
            //var p3 = {x:0,  y:80, r:80, z:0};
            //var p4 = trilaterate(p2,p3, p1, false)

            var p4 = trilaterate(p1, p3, p2);

            //console.log("p4 : \t" + JSON.stringify(p4));


            //           
            //    var context = myCanvas.getContext('2d');
            //    context.beginPath();
            //    context.arc(p4[1].y,p4[1].x,1,0,2*Math.PI,true); 
            //    context.strokeStyle = "blue";
            //    context.lineWidth = 1;
            //    context.stroke();
            //    context.closePath();

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   

            var S = (Math.pow(xc, 2.) - Math.pow(xb, 2.) + Math.pow(yc, 2.) - Math.pow(yb, 2.) + Math.pow(rb, 2.) - Math.pow(rc, 2.)) / 2.0;
            var T = (Math.pow(xa, 2.) - Math.pow(xb, 2.) + Math.pow(ya, 2.) - Math.pow(yb, 2.) + Math.pow(rb, 2.) - Math.pow(ra, 2.)) / 2.0;
            var y = ((T * (xb - xc)) - (S * (xb - xa))) / (((ya - yb) * (xb - xc)) - ((yc - yb) * (xb - xa)));
            var x = ((y * (ya - yb)) - T) / (xb - xa);

            //console.log("canvas.height: \t" + canvas.height + "\t canvas.width: \t" + canvas.width);
            //console.log("x: \t" + x + "\t y: \t" + y);


            // position  of receiver 
            var context = myCanvas.getContext('2d');
            context.beginPath();
            context.arc(y * (300 / 3.0), x * (150 / 2.4), 1, 0, 2 * Math.PI, true);
            context.strokeStyle = "red";
            context.lineWidth = 1;
            context.stroke();
            context.closePath();
            context.fill();

        }
    }

    function displayDistancestoiBeacons(beacons) {

        //        beacons.sort(function(a,b) {return (a.minor > b.minor) ? 1 : ((b.minor > a.minor) ? -1 : 0);} );
        //            //console.log('Array Lenght' + beacons.length);
        //            //console.log('beacon 0 - ' + beacons[0].minor);
        //            //console.log('beacon 1 - ' + beacons[1].minor);
        //            //console.log('beacon 2 - ' + beacons[2].minor);
        //            //console.log('beacon 3 - ' + beacons[3].minor);
        //console.log("Beacons" + JSON.stringify(beacons));

        //        $('#beaconInfo').empty();
        //        $('#jFyQ').empty();
        //        $('#CIsx').empty();
        //        $('#qSuD').empty();
        //        $('#m9Xa').empty();


        for (var i = 0; i < beacons.length; ++i) {
            var ibeaconinfo = $('<li>'
                 + 'Proximity: ' + beacons[i].proximity + '<br>'
                 + 'Distance: ' + beacons[i].accuracy + '<br>'
                 + 'RSSI: ' + beacons[i].rssi + '<br>'
                 + '</li>');

            //console.log("beacon Info: \t" + JSON.stringify(ibeaconinfo));
            //            //console.log(ibeaconinfo);

            switch (beacons[i].minor) {
                case '3':  // 'jFyQ'
                    //                    //console.log("im here in case 3");
                    $('#jFyQ').html(ibeaconinfo);
                    break;
                case '1': // 'CIsx'
                    $('#CIsx').html(ibeaconinfo);
                    break;
                case '2': //'qSuD'
                    $('#qSuD').html(ibeaconinfo);
                    break;
                case '4': // 'm9Xa'
                    //console.log("im here in case 4");
                    $('#m9Xa').html(ibeaconinfo);
                    break;
                default:
                    break;
            }

            //            $('#beaconInfo').replace(ibeaconinfo);
        }
    }

    var qx = 0;
    var qy = 0;

    function logData(beacons) {

        // set up GPS data

        navigator.geolocation.getCurrentPosition(onGeolocationSuccess, onGeolocationError);

        function onGeolocationSuccess(position) {
            //console.log('Im in GeolcoationSucess');
            var element = 'Latitude: ' + position.coords.latitude + '<br />' +
                                  'Accuracy: ' + position.coords.accuracy + '<br />' +
                                  'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '<br />' +
                                  'Altitude: ' + position.coords.altitude + '<br />' +
                                  'Heading: ' + position.coords.heading + '<br />' +
                                  'Longitude: ' + position.coords.longitude + '<br />' +
                                  'Speed: ' + position.coords.speed + '<br />' +
                                  'Timestamp: ' + position.timestamp + '<br />';
            $('#GeoLocation').html(element);

            accuracy = position.coords.accuracy;
            altitudeAccuracy = position.coords.altitudeAccuracy;
            altitude = position.coords.altitude;
            heading = position.coords.heading; mainLogger
            speed = position.coords.longitude;
            lattitude = position.coords.latitude;
            longitude = position.coords.longitude;
            timestamp = position.timestamp;
        }

        function onGeolocationError(error) {
            alert('code' + error.code + '\n' + 'message: ' + error.message + '\n');
        }

        if (beacons.length != 0) {
            for (var i = 0; i < beacons.length; ++i) {
                var beacon = beacons[i];
                if (mainLogger != null) {
                    mainLogger.log(csv(
                        timeStr(timestamp),
                        timestamp,
                        beacon.minor,
                        beacon.rssi,
                        beacon.accuracy,
                        lattitude,
                        longitude,
                        accuracy,
                        altitude,
                        altitudeAccuracy,
                        speed,
                        heading,
                        timestamp));
                }
            }
        }

    }


    function RorysWeightedSignalAverageMethod(beacons) {




        if (beacons.length != 0) {

            for (var i = 0; i < beacons.length; ++i) {
                ////console.log('i - \t' + i);
                var beacon = beacons[i];
                ////console.log('beacon' +JSON.stringify( beacon));
                switch (beacons[i].minor) {
                    case '1': // CLsX
                        d1 = Math.exp(beacon.rssi / 10.0);
                        y1 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    case '2': // qSud
                        d2 = Math.exp(beacon.rssi / 10.0);;
                        y2 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    case '3': // JfyQ
                        d3 = Math.exp(beacon.rssi / 10.0);;
                        y3 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    case '4': // JfyQ
                        d4 = Math.exp(beacon.rssi / 10.0);;
                        y4 = 1.0 / Math.sqrt(Math.abs(beacon.rssi));
                        break;
                    default:
                        d1 = 0;
                        d2 = 0;
                        d3 = 0;
                        d4 = 0;
                }
            }

        }

        var w1 = Math.sqrt(d1);
        var w2 = Math.sqrt(d2);
        var w3 = Math.sqrt(d3);
        var w4 = Math.sqrt(d4);

        var canvas = document.getElementById('myCanvas');

        var x1 = 0;
        var y1 = 0;
        var x2 = 2.4;
        var y2 = 0;
        var x3 = 0;
        var y3 = 3.0;
        var x4 = 2.4;
        var y4 = 3.0;

        qx = (w1 * x1 + w2 * x2 + w3 * x3 + w4 * x4) / (w1 + w2 + w3 + w4);

        qy = (w1 * y1 + w2 * y2 + w3 * y3 + w4 * y4) / (w1 + w2 + w3 + w4);

        //console.log("qx before : \t " + qx);
        //console.log("qy before : \t " + qy);

        qx = qx * (canvas.height / 2.4);

        qy = qy * (canvas.width / 3.0);

        //console.log("qx : \t " + qx);
        //console.log("qy : \t " + qy);




        var context = myCanvas.getContext('2d');
        context.beginPath();
        context.arc(qy, qx, 1, 0, 2 * Math.PI, true);
        context.strokeStyle = "red";
        context.lineWidth = 1;
        context.stroke();
        context.closePath();

    }

    var isRecording = false;

    // Binding to start recording
    $("#recordData").click(function () {



        if (isRecording == false) {
            navigator.geolocation.getCurrentPosition(geosucess, geoerror);

            function geosucess(position) {
                starttimestamp = moment.unix(position.timestamp / 1000).format("YYYY MMM DD   HH mm ss SS");
                mainLogger = logger([appFolderName, logFolderNAme, logFileName + ' ' + starttimestamp + '.txt'].join("/"));
                mainLogger.log(csv('minor', 'rssi', 'apiDistance', 'lattitude', 'longitude', 'l&laccuracy', 'altitude', 'altitudeAccuracy', 'speed', 'heading', 'timestamp'));
            }

            function geoerror(error) {
                alert('code' + error.code + '\n' + 'message: ' + error.message + '\n');
            }
            isRecording = true;
            //alert("Data is recording");
            console.log("data has started recording");

        }
    });

    // Binding to Stop recording data

    $("#recordDataStop").click(function () {



        if (isRecording == true) {
            isRecording = false;
            //alert("Data has stopped recording");
            console.log("data has stooped recording");
        }
    });

    $("#sync").click(function () {
        if (mainLogger != null) {
            var t = (new Date()).getTime();
            mainLogger.log(csv(timeStr(t), timestamp, "Sync Point"));
        }
        //sync = 'true';
    });


    function trilaterate(p1, p2, p3, return_middle) {
        // based on: https://en.wikipedia.org/wiki/Trilateration
        //console.log("Im in trilaterate");

        // some additional local functions declared here for
        // scalar and vector operations

        function sqr(a) {
            return a * a;
        }

        function norm(a) {
            return Math.sqrt(sqr(a.x) + sqr(a.y) + sqr(a.z));
        }

        function dot(a, b) {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        function vector_subtract(a, b) {
            return {
                x: a.x - b.x,
                y: a.y - b.y,
                z: a.z - b.z
            };
        }

        function vector_add(a, b) {
            return {
                x: a.x + b.x,
                y: a.y + b.y,
                z: a.z + b.z
            };
        }

        function vector_divide(a, b) {
            return {
                x: a.x / b,
                y: a.y / b,
                z: a.z / b
            };
        }

        function vector_multiply(a, b) {
            return {
                x: a.x * b,
                y: a.y * b,
                z: a.z * b
            };
        }

        function vector_cross(a, b) {
            return {
                x: a.y * b.z - a.z * b.y,
                y: a.z * b.x - a.x * b.z,
                z: a.x * b.y - a.y * b.x
            };
        }

        var ex, ey, ez, i, j, d, a, x, y, z, b, p4;

        ex = vector_divide(vector_subtract(p2, p1), norm(vector_subtract(p2, p1)));

        i = dot(ex, vector_subtract(p3, p1));
        a = vector_subtract(vector_subtract(p3, p1), vector_multiply(ex, i));
        ey = vector_divide(a, norm(a));
        ez = vector_cross(ex, ey);
        d = norm(vector_subtract(p2, p1));
        j = dot(ey, vector_subtract(p3, p1));

        x = (sqr(p1.r) - sqr(p2.r) + sqr(d)) / (2 * d);
        //console.log("tx : \t" + x);
        y = (sqr(p1.r) - sqr(p3.r) + sqr(i) + sqr(j)) / (2 * j) - (i / j) * x;
        //console.log("ty : \t" + y);
        b = sqr(p1.r) - sqr(x) - sqr(y);
        //console.log("tb : \t" + b);
        //    b = Math.abs(b);


        // floating point math flaw in IEEE 754 standard
        // see https://github.com/gheja/trilateration.js/issues/2
        if (Math.abs(b) < 0.0000000001) {
            b = 0;
        }

        z = Math.sqrt(b);
        //console.log("tz : \t" + z);

        // no solution found
        if (isNaN(z)) {
            return null;
        }

        a = vector_add(p1, vector_add(vector_multiply(ex, x), vector_multiply(ey, y)))
        p4a = vector_add(a, vector_multiply(ez, z));
        p4b = vector_subtract(a, vector_multiply(ez, z));

        if (z == 0 || return_middle) {
            return a;
        }
        else {
            return [p4a, p4b];
        }
    }

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();

