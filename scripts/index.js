﻿// A $( document ).ready() block.
$( document ).ready(function() {
    var element = document.getElementById("myCanvas");
    
    // read the width and height of the canvas
    var width = element.width;
    var height = element.height;

    var beaconRadius = 1.0;
    var fieldWidth = 60;
    var pixelsPerMetre = width/fieldWidth;
    var powerToIntensity = 10;

    function vectors() {
        function vec2(x, y) {
            return {x:x,y:y};
        }
        function dot2(x0,x1) {
            return x0.x*x1.x + x0.y*x1.y;
        }
        function add2(x0,x1) {
            return vec2(x0.x + x1.x, x0.y + x1.y);
        }  
        function sub2(x0,x1) {
            return vec2(x0.x - x1.x, x0.y - x1.y);
        }  
        return {
            vec2:vec2,
            dot2:dot2,
            add2:add2,
            sub2:sub2
        };
    }
    var vecs = vectors();

    function arrays() { 
        function add(arr0,arr1) {
            var sum = new Array(arr0.length);
            for (var i = 0; i < arr0.length; i++) {
                sum[i] = arr0[i] + arr1[i];
            }
            return sum;
        }
        function scale(arr,factor) {
            var scaled = new Array(arr.length);
            for (var i = 0; i < arr.length; i++) {
                scaled[i] = factor*arr[i];
            }
            return scaled;
        }
        function sum(arr) {
            var s = 0;
            for (var x of arr) s += x;
            return s;
        }
        function fill(arr, x) {
            for (var i = 0; i < arr.length; i++) {
                arr[i] = x;
            }
            return arr; 
        }
        function repeat(x, times) {
            return fill(new Array(times), x);
        }
        return {
            add:add,
            scale:scale,
            fill:fill,
            repeat:repeat,
            sum:sum
        };
    }
    var arrs = arrays();

    function beacon(id,position, transmitPower, colour) {
        function power(p) {
            var d = vecs.sub2(p,position);
            return transmitPower/vecs.dot2(d,d);
        }
        return  {
            id:id,
            position:position,
            colour:colour,
            transmitPower:transmitPower,
            power:power
        };
    }
    var beacons = [
        beacon(1, vecs.vec2( 5, 5), 1, [1,0,0,1]), // red 
        beacon(2, vecs.vec2( 5,-5), 1, [0,1,0,1]), // green
        beacon(3, vecs.vec2(-5,-5), 1, [0,0,1,1]), // blue
        beacon(4, vecs.vec2(-5, 5), 1, [1,1,0,1])  // yellow
    ];
    var expectedPowers = function(p) {
        return beacons.map(function (b) {return b.power(p);});
    } 
    var blendColours = function(colours, weights) {
        var col = null;
        var totalWeight = arrs.sum(weights);
        var normWeight = arrs.scale(weights, 1/totalWeight);
        for (var i = 0; i < colours.length; i++) {
            var c = arrs.scale(colours[i], normWeight[i]);
            col = (col != null)?arrs.add(col, c):c;
        } 
        return col;
    }
    var signalColour = function(powers){
        var colours = beacons.map(function (b) {return b.colour;});
        return blendColours(colours, powers);
    }
    var signalFunction = function(x,y){
        var p = vecs.vec2(x/pixelsPerMetre - fieldWidth/2, y/pixelsPerMetre - fieldWidth/2);
        return signalColour(expectedPowers(p));
    }
    function toByte(x) {
        return  x*0xFF & 0xFF;
    }

    // var measuredColour = function(powers){
    //     var colors = beacons.map(function (b) {return b.colour;});
    //     blendColours(colors,rssiTimeData.rssis);
    // }

    // var measuredFunction = function(x,y){
    //     var p = vecs.vec2(x/pixelsPerMetre - fieldWidth/2, y/pixelsPerMetre - fieldWidth/2);
    //     return signalColour(expectedPowers(p));
    // }
    function colourVecToRGBStr(col) {
        return "rgb(" + 
          toByte(col[0]) + "," +
          toByte(col[1]) + "," +
          toByte(col[2]) + ")";
    }
    function drawSample(pos, colour) {
        c.beginPath();
        c.fillStyle=colourVecToRGBStr(colour);
        c.strokeStyle="#000000";
        c.lineWidth = 0.1;
        c.arc(pos.x, pos.y, 0.5, 0, Math.PI*2, true);
        c.closePath();
        c.fill();
        c.stroke();
    }
    function drawMeasuredData(mData){
        for (var entry of mData) {
            var pos = entry.position;
            var signalPowers = entry.rssis.map(dBmTomW);
            var colours = beacons.map(function (b) {return b.colour;});
            if (entry.timestamp == 1485933185962) {
                console.log("found");
            }
            var blendedColour = blendColours(colours,signalPowers);
            //console.log("blendedColour", blendedColour);
            if (arrs.sum(blendedColour) < 1 || blendedColour.findIndex(isNaN) !== -1) {
                console.log("this shouldn't be black", blendedColour, signalPowers);
            }
            drawSample(pos, blendedColour);
        }    
    }


    function functionImageData(context, func, width, height) {
        var data = context.createImageData(width, height);
        var index = 0;
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                var v = func(x,y);
                data.data[index+0] = toByte(v[0]);//r
                data.data[index+1] = toByte(v[1]);//g
                data.data[index+2] = toByte(v[2]);//b
                data.data[index+3] = toByte(v[3]);//alpha
                index += 4;
            }
        }
        return data;
    }




    console.log("dimensions", width, height);

    

    // draw random dots
    // for (i = 0; i < 10000; i++) {
    //     x = Math.random() * width | 0; // |0 to truncate to Int32
    //     y = Math.random() * height | 0;
    //     r = Math.random() * 256 | 0;
    //     g = Math.random() * 256 | 0;
    //     b = Math.random() * 256 | 0;

    // for (var x = 0, y= 0; x <= width -1, y < height -1; x++, y++) {
    //     setPixel(imageData, x, y); // 255 opaque
    // }
    
    // copy the image data back onto the canvas
    
    var c = element.getContext("2d");
    
    var signalDistributionCanvas = document.createElement("canvas");
    signalDistributionCanvas.width = element.width;
    signalDistributionCanvas.height = element.height;
    var signalDistributionContext = signalDistributionCanvas.getContext("2d");
    var imageData = functionImageData(c, signalFunction, width, height);
    signalDistributionContext.putImageData(imageData, 0, 0); 



    console.log("transform", c.currentTransform);
    // create a new pixel array
    
    c.transform(pixelsPerMetre,0,0,-pixelsPerMetre,width/2,height/2);
    c.drawImage(signalDistributionCanvas, -fieldWidth/2, -fieldWidth/2, fieldWidth, fieldWidth); 
    // c.beginPath();
    // c.arc(1, 0, 5, 0, Math.PI*2, true);
    // c.closePath();
    // c.fill();

    // loading in the JSON file
    loadJsonData('5mradius_modified_angle.json');
    loadJsonData('15mradius.json');
    loadJsonData('25mradius.json');
    loadJsonData('12m_str_thru_cent.json');

    function loadJsonData(filename){
        var requestURL = String(filename);
        console.log("requestURL: \t", requestURL);
        var request = new XMLHttpRequest();
        request.open('GET', requestURL);
        request.responseType = 'json';
        request.send();
    
        request.onload  = function() {
            var jsondata = request.response;
            // console.log(JSON.stringify(jsondata));
            var beaconsData = populateBeaconData(jsondata);
            // console.log("beacons \t" + JSON.stringify(beaconsData,null, " "));
            // console.log("IM here ");
            var rssiTimeData = populateRssiTimeData(beacons, beaconsData);
            var tmp = drawMeasuredData(rssiTimeData);
             // console.log("rssiTimeData \t :" , JSON.stringify(rssiTimeData, null, "  "));
            // showHeroes(jsondata);
        }

    }


    function isDefined(x) {
        return typeof x != 'undefined'
    }
    function groupBy(elements, toKey) {
        var grouped = {};
        for (var e in elements) {
            var key = toKey(e);
            var current = grouped[key];
            if (isDefined(current)) {
                current = [];
                grouped[key] = current;
            }
            current.push(e);
        }
        return grouped;
    }
    function listEntiesAfter(entries, time) {
        var ls = []
        for (var i in entries) { // loops through lastEntries key value pair array
            var entry = entries[i]; 
            // console.log("Entry : \t" , entries[i])
            if (entry.timestamp >= time) ls.push(entry); //groups the entries according to timestamp 
        }
        return ls;
    }
    function groupDataByTimestamp(beaconsDataArr) {
        var record = [];
        var returnData = [];
        var maximumLookBack = 4000; // milli seconds
        var lastEntries = {}; // the last seen entrie grouped by minor
        var lastEntry = null;
        for (var entry of beaconsDataArr) {
            // console.log("entry = : ", entry); // JSON data in an array
            if (lastEntry != null && lastEntry.timestamp != entry.timestamp) {
                returnData.push([lastEntry, listEntiesAfter(lastEntries, lastEntry.timestamp - maximumLookBack)]);
            }
            lastEntries[entry.minor] = entry; // key value pair of entries based on minor as key
            lastEntry = entry;
        }
        if (lastEntry != null) returnData.push([lastEntry, listEntiesAfter(lastEntries, lastEntry.timestamp - maximumLookBack)]);
        return returnData;
    }
    function populateRssiTimeData(beacons, beaconsDataArr){
        return groupDataByTimestamp(beaconsDataArr).map(function (timeEntries) {
            var parent = timeEntries[0];
            var sub = timeEntries[1];
            var rssis = arrs.repeat(null, Math.max(beacons.length, sub.length));
            for (var e of sub) {
                rssis[e.minor - 1] = e.rssidB;
                // console.log( "rssis[" + (e.minor - 1) + "]\t - " + rssis[e.minor - 1] );
            }
            return {
                timestamp:parent.timestamp, 
                position:vecs.vec2(parent.x, parent.y),
                rssis:rssis
            };
        });
    }


    function dBmTomW(dBm) {
            return (dBm !== null)?10*Math.pow(10,dBm/10):0; // in mW
    }
    // function populateRssiTimeData(beaconsDataArr){
    //     var record = [];
    //     var returnData = [];
    //     for (var i = 1; i < beaconsDataArr.length; i++) {
    //         var previousEntry = beaconsDataArr[i-1]; 
    //         var entry = beaconsDataArr[i];
    //          // console.log("time \t :" + entry["timestamp"]);
    //          // console.log("time \t :" + previousEntry["timestamp"]);
    //         if (entry["timestamp"] == previousEntry["timestamp"]){
    //             // console.log("Im in the IF");
    //             record.push({
    //             timestamp : entry["timestamp"],
    //             minor : entry["minor"],
    //             rssidB : entry["rssidB"],
    //             sActual : entry["sActual"],
    //             angle : entry["angle"],
    //             x : entry["x"],
    //             y : entry["y"],
    //             rssiWatts : entry["rssiWatts"]  
    //         });
    //         }else{
    //             // before empty out the contents of record store 
    //             // it in an array of timestamps and beacon rssi
    //             var arr1 = [];
    //             for (var  i = 0; i < record.length; i++){
    //               var tmp = record[i];
    //               console.log("tmp \t : " + JSON.stringify(tmp));
    //               arr1.push(tmp["rssidB"]);
    //             };
    //             console.log("arr1: \t"+ JSON.stringify(arr1));
    //             returnData.push(
    //                 arr1
    //               );   
    //             record = [];
    //             record.push({
    //             timestamp : entry["timestamp"],
    //             minor : entry["minor"],
    //             rssidB : entry["rssidB"],
    //             sActual : entry["sActual"],
    //             angle : entry["angle"],
    //             x : entry["x"],
    //             y : entry["y"],
    //             rssiWatts : entry["rssiWatts"]  
    //             }); 
    //         }

    //     }
    //     // console.log(JSON.stringify(record));
    //     // // console.log(JSON.stringify(returnData));
    //     return returnData;
    //     // return 0;
    // }

    // function populateBeaconBean(jsonObj){
    //     var beacons = [];
    //     // console.log("entries : \t" + entries);
    //     for (var i = 0; i < jsonObj.length; i++) {
    //         var entry = jsonObj[i];
    //             beacons.push({
    //             timestamp : entry[0],
    //             minor : entry[1],
    //             rssidB : entry[2],
    //             sActual : entry[21],
    //             angle : entry[22],
    //             x : entry[23],
    //             y : entry[24],
    //             rssiWatts : entry[25]  
    //         });
    //     }
    //     return beacons;      
    // }
    function populateBeaconData(jsonObj){
        return jsonObj.map(function (entry) {
            return {
                timestamp : entry[0],
                minor : entry[1],
                rssidB : entry[2],
                sActual : entry[17],
                angle : entry[18],
                x : entry[19],
                y : entry[20],
                rssiWatts : entry[21]  
            };
        }); 
    }
    

    var beaconsData = [];
    

});
